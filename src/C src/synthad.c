//fonction de création de sinus
#include <stdio.h>
#include <stdlib.h>
#include "wav.h"
#include "afsk.h"

int main(int argc, char** argv){
    unsigned int t = 0;
    unsigned int* taille = &t;
    sig aprs = afsk_encode("RYRYRYRYRY 73 DE F4IEY Jules.", 1200, taille);
    wavwrite("afsk1200.wav", aprs, 48000, taille);
    free(aprs);
    return 0;
}
