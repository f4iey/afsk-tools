//Bibliotheque fonctions mainpulation WAV
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <stdint.h>
#include "wav.h"
#define TWO_PI 2*acos(-1.0)
struct Header {
	unsigned char riff[4];
	unsigned int file_size;
	unsigned char wave[4];
	unsigned char fmt[4];
	unsigned int length_format; //=16
	unsigned short type_format; //=1 pour PCM
	unsigned short channels; //nombre de canaux -> 1) mono 2) Stereo
	unsigned int Fe; //frequence d'échatillonnage
	unsigned int byterate; //nombre total de samples
	unsigned short block_align;
	unsigned short bps; //nombre de bits par échantillon (16)
	unsigned char data[4];
	unsigned int data_size; 
};

sig synthad(double a, double f, double T, unsigned int Fe, unsigned int* taille){
	//on prévoit la taille du fichier
    //a est compris entre -1 et 1
	double N = (double)Fe * T; //nombre de samples du fichier
	*taille = (unsigned int)N;
	double t;
	sig wav = malloc((*taille)*sizeof(int32_t)); //on initialise le fichier;
    assert(wav);
	for(t=0; t<N; t++){
        wav[(int)t] = a*INT_MAX*sin(TWO_PI*f*(t/Fe)); //on assemble la sinus
	}
	return wav;
}

sig appends(sig dest, sig from, unsigned int* tailled, unsigned int* taillef){
        //on append les deux signaux dans la mème variable et la taille de destination devient la nouvelle taille
        unsigned int oldSize = *tailled;
        *tailled += *taillef;
        sig appended = malloc((*tailled)*sizeof(int32_t));
        //assert(dest);
        int i;
        for (i = 0; i < oldSize; i++) {
            appended[i] = dest[i];
        }
        for (i = 0; i < *taillef; i++) {
            appended[oldSize + i] = from[i];
        }
        return appended;
}

int sigcmp(sig s1, sig s2, unsigned int* taille1, unsigned int* taille2){
    //renvoie 0 si les signaux sont identiques
    if(*taille1 != *taille2) return 1;
    int i;
    int n = *taille1;
    for(i=0; i<n; i++)
        if(s1[i] != s2[i]) return 1;
    return 0;
}

sig subsig(sig s, unsigned int min, unsigned int max, unsigned int* taille){
    //permet de créer des ss signaux en changeant les bornes min et max
    *taille = max - min + 1;
    sig out = malloc((*taille)*sizeof(int32_t));
    int i;
    for(i=0; i<(*taille); i++)
        out[i] = s[min + i];
    return out;
    
}

void wavwrite(char* filename, sig dataIn, unsigned int Fe, unsigned int* t){
	//fichier audio 32bits flottant
	//initialisation
	struct Header header;
	header.riff[0] = 'R';
	header.riff[1] = 'I';
	header.riff[2] = 'F';
	header.riff[3] = 'F';
	header.wave[0] = 'W';
	header.wave[1] = 'A';
	header.wave[2] = 'V';
	header.wave[3] = 'E';
	header.fmt[0] = 'f';
	header.fmt[1] = 'm';
	header.fmt[2] = 't';
	header.fmt[3] = ' ';
	header.data[0] = 'd';
	header.data[1] = 'a';
	header.data[2] = 't';
	header.data[3] = 'a';
	header.data_size = *(t)*sizeof(int32_t);
	header.file_size = header.data_size + 36; //taille totale du fichier - 8 octets
	header.length_format = 16;
	header.type_format = 1;
	header.channels = 1;
	header.Fe = Fe;
	header.bps = 32;
	header.byterate = (header.Fe * header.bps * header.channels) / 8;
	header.block_align = (header.bps * header.channels) / 8;
	FILE* audiofile = fopen(filename, "wb+"); //on cree le fichier
	//on met le header dans le fichier
	fwrite(header.riff, sizeof(header.riff), 1, audiofile); //riff
	fwrite(&header.file_size, sizeof(header.file_size), 1, audiofile); //file_size
	fwrite(header.wave, sizeof(header.wave), 1, audiofile); //wave
	fwrite(header.fmt, sizeof(header.fmt), 1, audiofile); //fmt
	fwrite(&header.length_format, sizeof(header.length_format), 1, audiofile);
	fwrite(&header.type_format, sizeof(header.type_format), 1, audiofile);
	fwrite(&header.channels, sizeof(header.channels), 1, audiofile);
	fwrite(&header.Fe, sizeof(header.Fe), 1, audiofile);
	fwrite(&header.byterate, sizeof(header.byterate), 1, audiofile);
	fwrite(&header.block_align, sizeof(header.block_align), 1, audiofile);
	fwrite(&header.bps, sizeof(header.bps), 1, audiofile);
	fwrite(header.data, sizeof(header.data), 1, audiofile);
	fwrite(&header.data_size, sizeof(header.data_size), 1, audiofile);
	//et on concatene la data
	int i;
	//int len = header.data_size / sizeof(header.data_size);
	for(i=0; i<(*t); i++){
        fwrite(&dataIn[i], sizeof(int32_t), 1, audiofile);
	}
	fclose(audiofile);
}
