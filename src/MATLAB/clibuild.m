%AFSK encoder Command line interface
%(c)2020 Jules F4IEY
clear all
close all
fprintf('Welcome in the AFSK CLI builder!\n(c)2020 Jules F4IEY\n\nEnter message to encode (ASCII only supported). Baudrate at 1200bits/s is highly recommended (max. 9600)\n');
message = input('message:','s');
baudrate = input('baudrate:');
%demander une confirmation de preview
req = input('preview sound before building audio [Y/N]?', 's');
fprintf('assembling data...\n');
res = getSoundMatrix(message, baudrate);
if strcmp(req, 'y') || strcmp(req, 'Y')
    soundsc(res, 48000);
end
fprintf('building audio...\n');
audiowrite(strcat(string(baudrate), '.wav'), res, 48000)
fprintf('Operation Completed!\nA wav file with the baudrate value as name should appear in folder (do not forget to rename if using CLI several times in a row).');